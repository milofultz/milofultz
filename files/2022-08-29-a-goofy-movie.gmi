"A Goofy Movie" is a classic movie. I watched this a zillion times as a kid, particularly loving all the music and dance scenes. But I never noticed how unusual all of the people are, particularly the families.

I first noticed that there was never more than one parent. Goofy, Pete, Roxanne's dad, the parents at Possum Park, the parents you see at the photo booth: why is there only ever one parent?

And then I noticed that there are no siblings, either. Max, Roxanne, PJ, Bobby, Goofy, Pete, Stacey. While we can't confirm most of these, it does seem to be consistent. There is one exception to this rule though, and that is that in different scenes, we do see twins and triplets. So we have single siblings, and multiple births. Huh.

Why is this? It seems peculiar that we see single parents and single children, especially once you start thinking longer term. How does any of this work?

# An Ad-Hoc Theory Of Life And Reproduction In The Goofy Universe

Well, first we need to get the obvious out of the way: the main population of this universe are humanoid/animaloid aliens. But you might be thinking "no, they are dogs/cats". But if that were true, why are they wearing clothes? Why are they speaking? Why are there not traditional parents? Why don't the other animals in this movie share these qualities?

## Home

It seems that they are on Earth, or some facsimile of Earth, considering the animals, machinery, structures, and land masses that are of Earth. At multiple times in the movie you see these creatures surviving such deadly situations as literal explosions, so they must be quite strong, and must be stronger than the humans that formerly inhabited the planet and were subsequently obliterated.

(Aside from Sasquatch, of course. One can only assume that these creatures are now so far removed from the time that people roamed the Earth, that the legend and lore of a "human" has probably become a grotesque scary story told around campfires. And considering his humanoid form, he probably strikes an unreasonable fear in them.)

The long time that has passed from their takeover to the current time of the movie is shown through the technology seen at the possum park, the terrible cars that explode, the faulty cassette decks, the photo studio, etc.

## Eggs

There is clearly romance within this universe. The entire movie hinges upon this connection between Max and Roxanne, but is hinted at through out, with scenes like Goofy getting flustered in the Powerline dressing room, Bobby and Stacy at the party, and the opening musical number.

So when one alien falls in love with another alien, one of them lays a batch of eggs through a kind of parthogenesis. Once this is complete, a battle to the death unfolds. Similar to the praying mantis or the octopus, this is a fight for nutrients and sustenance in consuming the losing party. The nutrients gained from the losing party are used to complete the parthenogenetic process, allowing the eggs to finish development.

## Hatching

At the end of their cycle, the eggs start to hatch. This is not a nice beautiful time, however, as similar to the cuckoo, the first one out of their shell proceeds to ensure their own survival by destroying the rest of the eggs. This explains why seemingly every single character in the movie is an only child: Max, PJ, Roxanne, etc.

This is obviously problematic for the long-term viability of this alien species; how does the population grow? There's no evidence of this, but it could be that there are multiple periods with multiple partners over time, thus leading to a greater than 1:1 growth. But what seems more likely is the one exception I have seen to the only child scenario. If more than one egg hatches at the same time, which is moderately unlikely, there will be no battle between the hatched children. Thus we end up with situations like the twins in the beginning musical numbers, and a (very very slightly) burgeoning population.

It's also easy to notice how terrible every single parent is in this movie. Pete, Goofy, Roxanne's dad, the parent of the kid getting their photo taken, etc. This makes sense, as the dominant traits are aggressiveness, power, and strength, via the life or death struggle post-birth through post-fertilization.

## The Long Game

The main plot point of the movie is that Max lies to Roxanne about how cool he is by knowing Powerline and going to the show in Los Angeles in person. He seemingly can't control this desire to show off, even though he has nothing to back it up. Why would he do this? Some would say love, sure, but I think this is a bit naïve.

As the more-or-less ultimate post-egg-laying battle is crucial to their existence, this kind of showboating is actually a calculated psychological play for power. If Max has connections to some of the most important people in their world, then he must be worth mating with? And if he finally kills and thus consumes Roxanne, surely he would be a good father? This was a risky move, for sure, but in showing that he actually followed through with his plan, he asserted a certain amount of power that allowed him to apologize later and not lose footing as the dominant one in the relationship.

Why would one partner be attracted to another who was more dominant if the former knew they would be killed by the latter? We can clearly see that having a relationship and children is stupid and risky, but necessary to pass on the genes and continue the species, and so is therefore done via instinct. Also note that former lovers are never talked about (why would we bring up the weaker of the two parents?).

## Conclusion

In conclusion, "A Goofy Movie" is not understood in the way that I believe the creators intended.

The movie is not a playful children's romp, but a story of a teenager coming of age and seeking security through psychological and physical violence, a true survival-of-the-fittest, the animated "Predator" of the 1990's.

