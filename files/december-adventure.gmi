# December Adventure

=> https://codeberg.org/milofultz/aoc2022 1130: Made a script that reads a file into memory and prints it to the console on uxn.
=> https://codeberg.org/milofultz/gemkill 1201: Updated my website and site generator to remove all old files pre-build.
=> https://codeberg.org/milofultz/gd-html-cleaner 1201: Created a quick browser tool in JS for my partner to partly automate their job.
=> https://codeberg.org/milofultz/cmus-random-songs 1202: Updated my random music picker from using VLC to using cmus.
=> https://codeberg.org/milofultz/tinybrainfans/src/branch/pages/upload 1203: Made a piece of code that updates my website via archiving it into a tarball and piping it to the server via ssh.
1204: Set up tmux and scripts for consistent work layouts; play with archivebox for funsies; use awk/sed to generate on-the-fly alternative docker-compose environments (yikes).
=> https://codeberg.org/milofultz/tod_sh 1205: Not much done today, onboarding all week at a new job; small reworks in my todo app.
=> tinybrain.fans/dotfiles.html 1206: Revamped dotfiles to work over multiple computers.
=> https://codeberg.org/milofultz/gforth-dump 1207: Started dinking around with gforth. Not my favorite as the documentation is kind of bad, but it's enough to get me started understanding forth. Got helloworld.fs done.
1208: Spent some time annotating and trying to understand existing forth examples, like a `cat` program. It's funny how having a totally different erlang[1] can just throw me for a loop in trying to grok how these programs work.
1209-1210: Not a lot of juice to spend time on a computer doing things after a long week of onboarding at a new job plus a flu shot PLUS the newest C19 booster.
=> https://codeberg.org/milofultz/gforth-dump/src/branch/main/memory.fs 1211: Learned how to finally read data from a file and print it to the screen! It was a huge PITA, but it was fun to try and solve something like this considering that the internet has like nothing out there to help you. Good puzzle!
1212-1216: Still onboarding with lots of zoom calls... (_ _") .. zzz
1217-1218: Started dogsitting and didn't have a lot of energy. Think I'm still a bit overwhelmed from so many zoom calls and staring at a screen getting restless.
1219-1220: Barf, more zoom calls and bonux running around to prep for some real nasty weather around here.
=> https://tinybrain.fans/aria-labels.html 1221: Started digging into the deep world of ARIA labels. Stoked to be able to get deep with some accessibility at my new gig.
=> https://codeberg.org/milofultz/gforth-dump/src/branch/main/cal.fs 1222: Wanted to play some more with Forth, so implemented a date-to-weekday converter. Wasn't too difficult, but the end goal of this will be hopefully some kind of unix `cal`[2] kind of calendar printer.
=> https://codeberg.org/milofultz/gforth-dump/src/branch/main/print-cal.fs 1223: Was able to print a unix-style `cal` pretty quickly! Stack manipulation is still very difficult and weird to hold in my brain, but it's becoming much more fluent to think of where I want to go and then make the code to get there.
=> https://codeberg.org/milofultz/aoc2022/src/branch/main/1/1.fs Also solved day one of Advent of Code[3] in Forth, so that was fun.
=> https://codeberg.org/milofultz/swiki_sh 1224: Started working on porting my static site generator {{SWIKI}}[4] from Python over to shell scripts, mainly using awk. I don't know if it will be better or worse, but it will have less dependencies and I get to muck around in shell scripting more, along with some TDD along the way.
=> https://codeberg.org/milofultz/swiki_sh 1225: Continued working on my shell static site generator. Playing around with Forth gave me a lot more ideas in how to structure things. More metaprogramming in terms of good unit-tested funcitons, kinda like building with legos.
=> https://codeberg.org/milofultz/swiki_sh 1226-1227: Same same!
1228: Mostly work-related stuff today, but did have fun creating a Vim command to get the canonical URL of a file in a work repo today. Lots of weird git shell commands made a very useful command, at least for sharing what I'm looking at with colleagues!


=> https://madhadron.com/programming/seven_ur_languages.html 1: The seven programming ur-languages
=> https://man7.org/linux/man-pages/man1/cal.1.html 2: cal(1)
=> https://adventofcode.com/ 3: Advent of Code
=> https://codeberg.org/milofultz/swiki 4: {{SWIKI}}

