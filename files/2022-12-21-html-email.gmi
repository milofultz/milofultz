# *sigh* Lastpass

In setting up a new password manager (Lastpass, which I wouldn't recommend for other reasons besides this silly story), I got an email containing a temporary password of `1l`. This seemed like a wildly short temporary password, but I don't know what they're doing behind the scenes.

I try and sign up and nothing worked: "Your temporary password is wrong." Okaaaaaay.

So I open the email's source, suspecting this is some stupid stuff after managing email formatting problems at my last job, to find out that the temporary password contained an unescaped `<`. So the end of my temporary password was rendering as an HTML element.

This is annoying, but not terrible. However, for people that aren't as tech savvy, this really sucks. I can imagine the support email: "Make sure you are typing the temporary password correctly."

So I go to their support page, screenshots at the ready, and draft a message on their form. And the form requires Javascript. Makes sense. And they only accept one file. Alright, put screenshots into a PDF. And the form errors out on send.

It really didn't inspire a lot of faith in Lastpass, not that I had a lot anyway, considering all their security incidents. But when the support form is broken and they're not escaping HTML entities in their emails, it just feels like amateur hour.

EDIT: And of course, the day after writing this, Lastpass announces some seriously bad news about a recent breach, wherein they bungled communications with their users saying that this hack wasn't so bad, but in actuality it was a leak of user's password vaults. If the hackers can get the master password, *they can get everything*. Yikes, don't use Lastpass, use 1password or something else.

=> https://www.theverge.com/2022/12/22/23523322/lastpass-data-breach-cloud-encrypted-password-vault-hackers Hackers stole encrypted LastPass password vaults, and we’re just now hearing about it
