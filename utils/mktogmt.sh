for file in *.gmi; do
  start=$(cat "$file")
  while true; do
    # 1. Replace Markdown links with static text and put them at the end of the line as gemtext links
    # 2. Normalize all Markdown list items to '*'
    # 3. Convert all numbered lists to '*'
    end=$(echo "$start" | \
      sed -e 's/\([^[]*\)\[\([^]]*\)\](\([^)]*\))\(.*\)/\1\2\4 ¡=> \3 \2¡/' | \
      sed -e 's/^- /\* /' | \
      sed -e 's/^[[:digit:]]\{1,\}\. /\* /'\
    )
    if [ "$start" != "$end" ]; then
      # Continue because all links are not converted yet
      start="$end"
    else
      # Convert inverted exclam delimiter to newlines and write to file
      echo "${end//¡/\n\n}" > "${file}"
      break
    fi
  done
done

